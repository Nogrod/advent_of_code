use std::collections::HashMap;
use std::time::Instant;
use clap::{App, Arg, ArgMatches};
use itertools::Itertools;
use crate::dor_trait::EntryPoint;
use crate::helper::{parse_to_i32, parse_to_usize};

mod dor_1;
mod dor_2;
mod dor_3;
mod dor_4;
mod dor_5;
mod dor_6;
mod dor_7;
mod dor_8;
mod dor_9;
mod dor_10;
mod dor_11;
mod dor_12;
mod dor_13;
mod dor_14;
mod dor_15;

mod helper;
mod dor_trait;

fn main() {
    let dor_list: Vec<Box<dyn EntryPoint>> = vec![
        Box::new(dor_1::Dor {}),
        Box::new(dor_2::Dor {}),
        Box::new(dor_3::Dor {}),
        Box::new(dor_4::Dor {}),
        Box::new(dor_5::Dor {}),
        Box::new(dor_6::Dor {}),
        Box::new(dor_7::Dor {}),
        Box::new(dor_8::Dor {}),
        Box::new(dor_9::Dor {}),
        Box::new(dor_10::Dor {}),
        Box::new(dor_11::Dor {}),
        Box::new(dor_12::Dor {}),
        Box::new(dor_13::Dor {}),
        Box::new(dor_14::Dor {}),
        Box::new(dor_15::Dor {}),
    ];
    let mut solutions: HashMap<String, Box<dyn EntryPoint>> = HashMap::new();
    for (i, dor) in dor_list.into_iter().enumerate() {
        solutions.insert((i + 1).to_string(), dor);
    }

    let matches = App::new("Advent of Code")
        .version("1.0")
        .author("Manuel Luther")
        .about("Execute a solution of a day, from the 'Advent of Code'")
        .arg(Arg::with_name("day")
            .value_name("day")
            .help("Sets a custom config file")
            .takes_value(true)
            .required(false)
            .conflicts_with("all")
            .index(1))
        .arg(Arg::with_name("solution")
            .help("Sets the input file to use")
            .required(false)
            .takes_value(true)
            .conflicts_with("all")
            .index(2))
        .arg(Arg::with_name("time")
            .long("time")
            .takes_value(false)
            .required(false)
            .help("print the execution time"))
        .arg(Arg::with_name("hide")
            .long("hide")
            .takes_value(false)
            .required(false)
            .help("Show the Solution"))
        .arg(Arg::with_name("all")
            .long("all")
            .takes_value(false)
            .required(false)
            .help("Show the Solution"))
        .get_matches();

    if matches.is_present("all") {
        for name in solutions.keys().sorted_by(|x, y| parse_to_i32(x).cmp(&parse_to_i32(y))){
            let day = solutions.get(name).unwrap();
            let result_output = execute_day_part_x(day, "1", &matches);
            print_output(name, result_output);

            let result_output = execute_day_part_x(day, "2", &matches);
            print_output(name, result_output);
        }
    } else if matches.is_present("day") && matches.is_present("solution") {
        let part = matches.value_of("solution").unwrap();
        let name = matches.value_of("day").unwrap();
        let day = &solutions[name];

        let result_output = execute_day_part_x(day, part, &matches);
        println!("Solution Day {}: {} ", name, result_output);
    } else if matches.is_present("day") {
        let name = matches.value_of("day").unwrap();
        let day = &solutions[name];

        let result_output = execute_day_part_x(day, "1", &matches);
        println!("Solution Day {}: {} ", name, result_output);

        let result_output = execute_day_part_x(day, "2", &matches);
        println!("Solution Day {}: {} ", name, result_output);
    } else {
        let name= solutions.keys().into_iter().map(|x| parse_to_usize(x)).sorted().max().unwrap().to_string();
        let day = solutions.get(&name).unwrap();
        let result_output = execute_day_part_x(day, "1", &matches);
        println!("Solution Day {}: {} ", name, result_output);
        let result_output = execute_day_part_x(day, "2", &matches);
        println!("Solution Day {}: {} ", name, result_output);
    }
}

fn print_output(day: &str, result_output: String) {
    println!("Day {:<2}: {} ", day, result_output);

}

fn execute_day_part_x(day: &Box<dyn EntryPoint>, part: &str, matches: &ArgMatches) -> String {
    let do_benchmark = matches.is_present("time");

    let start = Instant::now();
    let result = match part {
        "1" => day.part_1(),
        "2" => day.part_2(),
        _ => "Nor a valid Part for a dor".to_string(),
    };

    let mut result_output = String::new();
    if !matches.is_present("hide") {
        result_output.push_str(&format!("{:<14} ", result))
    }
    if do_benchmark {
        let elapsed = start.elapsed();
        result_output.push_str(&format!("|  Execution Time: {:.2?} ", elapsed));
    }
    result_output
}

