#[cfg(test)]
mod tests {
    use crate::dor_15::Dor;
    use crate::dor_trait::EntryPoint;

    #[test]
    fn test_solution_1() {
        let problem = Dor {};
        assert_eq!(&problem.part_1(), "540")
    }

    #[test]
    fn test_solution_2() {
        let problem = Dor {};
        assert_eq!(&problem.part_2(), "2879")
    }
}