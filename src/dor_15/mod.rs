use std::collections::HashMap;
use itertools::Itertools;
use petgraph::{Directed, Graph};
use petgraph::graph::NodeIndex;
use crate::dor_trait::EntryPoint;
use crate::helper;
use crate::helper::parse_to_usize;
use petgraph::algo::dijkstra;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(15);

        let mut cave_system_array = vec![];
        for row in inputs {
            cave_system_array.push(row.chars().map(|element| parse_to_usize(&element.to_string())).collect_vec());
        }

        let cave_system = CaveSystem::from_vec(cave_system_array);

        let cost = cave_system.get_lowest_risk_level();

        format!("{}", cost)
    }

    fn part_2(&self) -> String {
        let inputs = helper::get_lines_from_file(15);

        let mut cave_system_array = vec![];

        let inputs = inputs.collect_vec();
        for y_offset in 0..5 {
            for row in inputs.iter() {
                let mut carve_row = vec![];
                for x_offset in 0..5 {
                    carve_row.extend(
                        row.chars()
                            .map(|element| {
                                let mut risk_level = parse_to_usize(&element.to_string()) + y_offset + x_offset;
                                if risk_level.gt(&9) {
                                    risk_level = (risk_level % 10) + 1
                                }
                                risk_level
                            })
                            .collect_vec());
                }
                cave_system_array.push(carve_row);
            }
        }
        let cave_system = CaveSystem::from_vec(cave_system_array);

        let cost = cave_system.get_lowest_risk_level();

        format!("{}", cost)
    }
}


struct CaveSystem {
    graph: Graph<(usize, usize), usize, Directed>,
    nodes: HashMap<(i32, i32), (NodeIndex, usize)>,
}

impl CaveSystem {
    pub fn from_vec(vector: Vec<Vec<usize>>) -> CaveSystem {
        let mut nodes = HashMap::new();
        let mut cave_system = Graph::new();
        for (y, row) in vector.iter().enumerate() {
            for (x, cave) in row.iter().enumerate() {
                let node = cave_system.add_node((x, y));
                nodes.insert((x as i32, y as i32), (node, *cave));
            }
        }
        let mut cave_system = CaveSystem { nodes, graph: cave_system };
        for (y, row) in vector.iter().enumerate() {
            let y = y as i32;
            for (x, _) in row.iter().enumerate() {
                let x = x as i32;
                cave_system.add_ege(&(x, y), -1, 0);
                cave_system.add_ege(&(x, y), 1, 0);
                cave_system.add_ege(&(x, y), 0, 1);
                cave_system.add_ege(&(x, y), 0, -1);
            }
        }

        cave_system
    }

    fn add_ege(&mut self, node_1: &(i32, i32), x_diff: i32, y_diff: i32) {
        let node_index_1 = self.nodes.get(node_1).unwrap();
        match self.nodes.get(&(node_1.0 - x_diff, node_1.1 - y_diff)) {
            None => {}
            Some((node_index_2, risk_level)) => { self.graph.add_edge(node_index_1.0, *node_index_2, *risk_level); }
        }
    }

    fn get_lowest_risk_level(&self) -> usize {
        let start = NodeIndex::new(0);
        let end = Some(NodeIndex::new(self.graph.node_count() - 1));
        let path = dijkstra(&self.graph, start, end, |x| x.weight().to_owned());
        return *path.get(&end.unwrap()).unwrap();
    }
}

