use std::collections::{HashMap, HashSet};
use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(10);

        let lines: Vec<String> = inputs.collect();

        let mut error_score = 0;
        let error_point_mapping = HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);

        let closing_chars = HashSet::from([')', '}', ']', '>']);
        let bracket_mapping = HashMap::from([(')', '('), ('}', '{'), (']', '['), ('>', '<'), ('(', ')'), ('{', '}'), ('[', ']'), ('<', '>')]);
        for line in lines {
            let mut chunk_stack: Vec<char> = vec![];
            for element in line.chars() {
                if closing_chars.contains(&element) {
                    if chunk_stack.is_empty() {
                        println!("Error");
                        break;
                    } else if !chunk_stack.last().unwrap().eq(bracket_mapping.get(&element).unwrap()) {
                        error_score += error_point_mapping.get(&element).unwrap();
                        break;
                    } else {
                        chunk_stack.pop();
                    }
                } else {
                    chunk_stack.push(element);
                }
            }

        }
        format!("{}", error_score)
    }

    fn part_2(&self) -> String {
        // lets goooo code duplication. WET is best! https://en.wikipedia.org/wiki/Don't_repeat_yourself#WET
        let inputs = helper::get_lines_from_file(10);

        let lines: Vec<String> = inputs.collect();

        let mut incomplete_scores = vec![];
        let incomplete_point_mapping = HashMap::from([('(', 1), ('[', 2), ('{', 3), ('<', 4)]);

        let closing_chars = HashSet::from([')', '}', ']', '>']);
        let bracket_mapping = HashMap::from([(')', '('), ('}', '{'), (']', '['), ('>', '<'), ('(', ')'), ('{', '}'), ('[', ']'), ('<', '>')]);
        for line in lines {
            let mut chunk_stack: Vec<char> = vec![];
            let mut has_error = false;
            for element in line.chars() {
                if closing_chars.contains(&element) {
                    if chunk_stack.is_empty() {
                        println!("Error");
                        break;
                    } else if !chunk_stack.last().unwrap().eq(bracket_mapping.get(&element).unwrap()) {

                        has_error = true;
                        break;
                    } else {
                        chunk_stack.pop();
                    }
                } else {
                    chunk_stack.push(element);
                }
            }
            if has_error {
                continue;
            }
            let mut points: i64 = 0;
            for missing_element in chunk_stack.iter().rev() {
                points *= 5;
                points += incomplete_point_mapping.get(missing_element).unwrap();

            }
            incomplete_scores.push(points);
        }
        incomplete_scores.sort();
        format!("{}", incomplete_scores[incomplete_scores.len() / 2])
    }
}

