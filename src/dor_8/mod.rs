use std::collections::HashMap;
use crate::dor_trait::EntryPoint;
use crate::helper;
use crate::helper::parse_to_usize;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(8);

        let count_numbers: Vec<usize> = vec![2, 3, 4, 7];
        let mut count_1_4_7_8 = 0;
        for line in inputs {
            let signal = Signal::new(line);
            let a = signal.count_lengths_output_signal(&count_numbers);
            count_1_4_7_8 += a;
        }
        format!("{}", count_1_4_7_8)
    }

    fn part_2(&self) -> String {
        let inputs = helper::get_lines_from_file(8);
        let mut sum = 0;
        for line in inputs {
            let mut signal = Signal::new(line);
            sum +=  signal.calculate_digits();
        }

        format!("{}", sum)
    }
}

#[derive(Debug)]
struct Signal {
    output_signal: Vec<String>,
    input_signal: Vec<String>,
}

impl Signal {
    pub fn new(raw_string: String) -> Signal {
        let parts: Vec<&str> = raw_string.split('|').collect();
        let output = parts[1].split_whitespace().map(|x| helper::sort_string(x)).collect();
        let input = parts[0].split_whitespace().map(|x| helper::sort_string(x)).collect();
        Signal {
            output_signal: output,
            input_signal: input,
        }
    }

    pub fn count_lengths_output_signal(&self, lengths: &Vec<usize>) -> usize {
        let mut n = 0;
        for signal_part in &self.output_signal {
            if lengths.contains(&signal_part.len()) {
                n += 1;
            }
        }
        return n;
    }

    pub fn calculate_digits(&mut self) -> usize {
        let mapping = self.solve();
        let mut all_digits = String::new();
        for signal in &self.output_signal {
            let num = mapping.get(signal).unwrap();
            all_digits.push_str(&num.to_string())
        }

        return parse_to_usize(&all_digits);
    }

    pub fn solve(&mut self) -> HashMap<String, i32> {
        let mut mapping = HashMap::new();
        let mut to_remove = vec![];

        // get 1, 7, 4 and 8
        for (i, signal) in (&self).input_signal.iter().enumerate() {
            match signal.len() {
                2 => {
                    mapping.insert(1, String::from(signal));
                    to_remove.push(i)
                }
                3 => {
                    mapping.insert(7, String::from(signal));
                    to_remove.push(i)
                }
                4 => {
                    mapping.insert(4, String::from(signal));
                    to_remove.push(i)
                }
                7 => {
                    mapping.insert(8, String::from(signal));
                    to_remove.push(i)
                }
                _ => {}
            }
        }
        while to_remove.len().gt(&0) {
            self.input_signal.remove(to_remove.pop().unwrap());
        }

        // get 6 and candidates for 9 and 0
        let mut to_remove = vec![];
        let mut six_or_zero = vec![];
        let one_seven_segment: Vec<char> = mapping.get(&1).unwrap().chars().collect();
        for (i, signal) in (&self).input_signal.iter().enumerate() {
            if signal.len().eq(&6) {
                // check if it is a 6
                if signal.contains(one_seven_segment[0]) && !signal.contains(one_seven_segment[1]) || !signal.contains(one_seven_segment[0]) && signal.contains(one_seven_segment[1]) {
                    mapping.insert(6, String::from(signal));
                    to_remove.push(i)
                } else {
                    six_or_zero.push(signal);
                    to_remove.push(i)
                }
            }
        }
        let candidate_1 = subtract_string(six_or_zero[0], six_or_zero[1]);

        if mapping.get(&4).unwrap().contains(&candidate_1) {
            mapping.insert(9, String::from(six_or_zero[0]));
            mapping.insert(0, String::from(six_or_zero[1]));
        } else {
            mapping.insert(9, String::from(six_or_zero[1]));
            mapping.insert(0, String::from(six_or_zero[0]));
        }
        while to_remove.len().gt(&0) {
            self.input_signal.remove(to_remove.pop().unwrap());
        }
        let mut to_remove = vec![];

        // get 3
        for (i, signal) in (&self).input_signal.iter().enumerate() {

            if signal.contains(one_seven_segment[0]) && signal.contains(one_seven_segment[1]) {
                mapping.insert(3, String::from(signal));
                to_remove.push(i)
            }
        }
        while to_remove.len().gt(&0) {
            self.input_signal.remove(to_remove.pop().unwrap());
        }

        // get 2 and 5
        let potential_2 = subtract_string(&self.input_signal[0], mapping.get(&6).unwrap());
        if potential_2.len().eq(&1) {
            mapping.insert(2, String::from(&self.input_signal[0]));
            mapping.insert(5, String::from(&self.input_signal[1]));
        } else {
            mapping.insert(2, String::from(&self.input_signal[1]));
            mapping.insert(5, String::from(&self.input_signal[0]));
        }

        // change key and values
        let mut final_mapping = HashMap::new();
        for (key, value) in mapping {
            final_mapping.insert(value, key);
        }
        return final_mapping;
    }
}

fn subtract_string(left: &str, right: &str) -> String {
    let mut left = String::from(left);
    for element in right.chars() {
        left = left.replace(element, "");
    }
    return left
}
