use std::collections::HashSet;

use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(9);
        let height_map = HeightMap::from_vec(inputs.map(|x| helper::to_u32_list(&x)).collect());

        let low_points = height_map.get_low_points();
        let low_points_values: Vec<usize> = low_points
            .iter()
            .map(|(x, y)| height_map.get_element(*x, *y).unwrap())
            .collect();
        format!("{:?}", low_points_values.iter().sum::<usize>() + low_points.len())
    }

    fn part_2(&self) -> String {
        let inputs = helper::get_lines_from_file(9);
        let height_map = HeightMap::from_vec(inputs.map(|x| helper::to_u32_list(&x)).collect());

        let low_points = height_map.get_low_points();
        let mut basin_sizes = vec![];
        for point in low_points {
            let mut basin = Basin::new();
            basin.build_basin(&height_map, point.0 as i32, point.1 as i32);
            basin_sizes.push(basin.get_size());
        }
        basin_sizes.sort();
        format!("{}", basin_sizes.pop().unwrap() * basin_sizes.pop().unwrap() * basin_sizes.pop().unwrap())
    }
}

struct HeightMap {
    height_map: Vec<Vec<u32>>,
}

impl HeightMap {
    pub fn from_vec(height_map: Vec<Vec<u32>>) -> HeightMap {
        HeightMap {
            height_map,
        }
    }

    pub fn greater_then(&self, first_index: i32, second_index: i32, other: u32) -> bool {
        if first_index.lt(&0) || second_index.lt(&0) {
            return true;
        }
        let result =
            match self.height_map.get(first_index as usize) {
                None => true,
                Some(row) => {
                    match row.get(second_index as usize) {
                        None => true,
                        Some(element) => { element.gt(&other) }
                    }
                }
            };
        result
    }

    pub fn get_low_points(&self) -> Vec<(usize, usize)> {
        let mut low_points = vec![];
        for (y, line) in self.height_map.iter().enumerate() {
            let y = y as i32;
            for (x, element) in line.iter().enumerate() {
                let x = x as i32;
                if {
                    self.greater_then(y - 1, x, *element) &&
                        self.greater_then(y + 1, x, *element) &&
                        self.greater_then(y, x - 1, *element) &&
                        self.greater_then(y, x + 1, *element)
                } { low_points.push((x as usize, y as usize)) }
            }
        }
        low_points
    }

    pub fn get_element(&self, index_x: usize, index_y: usize) -> Option<usize> {

        match self.height_map.get(index_y as usize) {
            None => None,
            Some(row) => {
                match row.get(index_x as usize) {
                    None => None,
                    Some(element) => Some(*element as usize)
                }
            }
        }
    }

    pub fn get_element_from_i32(&self, index_x: i32, index_y: i32) -> Option<usize> {
        if index_x.lt(&0) || index_y.lt(&0) {
            return None;
        }
        match self.height_map.get(index_y as usize) {
            None => None,
            Some(row) => {
                match row.get(index_x as usize) {
                    None => None,
                    Some(element) => Some(*element as usize)
                }
            }
        }
    }
}

#[derive(Debug)]
struct Basin {
    in_basin: HashSet<(i32, i32)>,
    basin_border: HashSet<(i32, i32)>,
}

impl Basin {
    pub fn new() -> Basin {
        Basin {
            in_basin: HashSet::new(),
            basin_border: HashSet::new(),
        }
    }

    pub fn get_size(&self) -> usize {
        self.in_basin.len()
    }

    pub fn build_basin(&mut self, height_map: &HeightMap, index_x: i32, index_y: i32) {
        if self.in_basin.contains(&(index_x, index_y)) {
            return;
        }
        match height_map.get_element_from_i32(index_x, index_y) {
            None => { return; }
            Some(9) => { self.basin_border.insert((index_x, index_y)); }
            Some(_) => {
                self.in_basin.insert((index_x, index_y));
                self.build_basin(height_map, index_x, index_y + 1);
                self.build_basin(height_map, index_x, index_y - 1);
                self.build_basin(height_map, index_x + 1, index_y);
                self.build_basin(height_map, index_x - 1, index_y);
            }
        };
    }
}
