use std::collections::HashSet;
use crate::dor_trait::EntryPoint;
use crate::helper;
use crate::helper::to_u32_list;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(11);
        let mut octopus_grid = OctopusGrid::from_vec(inputs.map(|x| to_u32_list(&x)).collect());

        for _ in 0..100 {
            octopus_grid.step();
        }

        format!("{}", octopus_grid.get_flashes())
    }

    fn part_2(&self) -> String {

        let inputs = helper::get_lines_from_file(11);
        let mut octopus_grid = OctopusGrid::from_vec(inputs.map(|x| to_u32_list(&x)).collect());

        let max_iterations = 100000;
        let mut step_all_flashes = 0;
        for i in 0..max_iterations {
            octopus_grid.step();
            if octopus_grid.flashes_all() {
                step_all_flashes = i + 1;
                break
            }
        }

        format!("{}", step_all_flashes)
    }
}


struct OctopusGrid {
    grid: Vec<Vec<u32>>,
    x_length: usize,
    y_length: usize,
    flashes: usize,
}

impl OctopusGrid {

    fn flashes_all(&self) -> bool {
        for row in self.grid.iter() {
            for octopus in row {
                if !octopus.eq(&0) {
                    return false
                }
            }
        }
        return true;
    }

    fn get_flashes(&self) -> usize {
        self.flashes
    }

    pub fn from_vec(grid: Vec<Vec<u32>>) -> OctopusGrid {
        let x_length = grid[0].len();
        let y_length = grid.len();
        OctopusGrid { grid, x_length, y_length, flashes: 0 }
    }

    pub fn step(&mut self) {
        self.add_to_grid();
        let mut visits: HashSet<(usize, usize)> = HashSet::new();
        for y in 0..self.y_length {
            for x in 0..self.x_length {
                self.update(x, y, &mut visits);
            }
        }
        self.reset();
    }
    fn update(&mut self, x: usize, y: usize, visits: &mut HashSet<(usize, usize)>) {
        if visits.contains(&(x, y)) {
            return;
        }

        if self.grid[y][x].gt(&9) {
            visits.insert((x, y));

            for neighbor_x in (x as i32 - 1)..(x as i32 + 2) {
                for neighbor_y in (y as i32 - 1)..(y as i32 + 2) {
                    if neighbor_x.lt(&0) || neighbor_y.lt(&0) || neighbor_x.ge(&(self.x_length as i32)) || neighbor_y.ge(&(self.y_length as i32))  {
                        continue;
                    }
                    self.grid[neighbor_y as usize][neighbor_x as usize] += 1;
                    self.update(neighbor_x as usize, neighbor_y as usize, visits);
                }
            }
        }
    }
    fn add_to_grid(&mut self) {
        for row in (&mut self.grid).iter_mut() {
            for octopus in row.iter_mut() {
                *octopus += 1;
            }
        }
    }

    fn reset(&mut self) {
        for y in 0..self.y_length {
            for x in 0..self.x_length {
                if self.grid[y][x].gt(&9) {
                    self.grid[y][x] = 0;
                    self.flashes += 1;
                }
            }
        }
    }
}

#[allow(dead_code)]
fn print_octopus_grid(octopus_grid: &OctopusGrid) {
    println!("Num flashes: {}", octopus_grid.flashes);
    for row in &octopus_grid.grid {
        println!("{:?}", row);
    }
}