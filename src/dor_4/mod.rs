use crate::dor_trait::EntryPoint;
use crate::helper;
use std::convert::TryInto;
use std::ops::Not;
use std::option::Option;
use crate::helper::parse_to_usize;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs: Vec<String> = helper::get_lines_from_file(4).collect();

        let mut boards = Board::parse_boards(&inputs[1..]);
        let picked_numbers = get_picked_numbers(&inputs);

        for picked in picked_numbers {
            for board in &mut boards {
                if board.select(picked) {
                    return format!("{}", board.calculate_score(picked));
                }
            }
        }
        format!("FAIL")
    }

    fn part_2(&self) -> String {
        let inputs: Vec<String> = helper::get_lines_from_file(4).collect();

        let mut boards = Board::parse_boards(&inputs[1..]);
        let picked_numbers = get_picked_numbers(&inputs);

        let mut last_winning_score = 0;
        for picked in picked_numbers {
            let mut i = 0;
            let mut boards_to_remove = vec![];

            for board in &mut boards {
                if board.select(picked) {
                    last_winning_score = board.calculate_score(picked);
                    boards_to_remove.push(i);
                }
                i += 1;
            }
            boards_to_remove.sort_by(|a, b| b.cmp(a));
            for i in boards_to_remove {
                boards.remove(i);
            }
        }
        format!("{}", last_winning_score)
    }
}

fn get_picked_numbers(inputs: &Vec<String>) -> Vec<usize> {
    inputs[0].split(",").map(|x| parse_to_usize(x)).collect()
}


#[derive(Debug)]
struct Board {
    rows: [[usize; 5]; 5],
    selected: [[bool; 5]; 5],
}

impl Board {
    fn parse_boards(inputs: &[String]) -> Vec<Board> {
        let mut boards = vec![];
        let mut i = 0;
        while i.lt(&inputs.len()) {
            if inputs[i].eq("") {
                let mut builder = BoardBuilder::new();
                builder.add_rows(&inputs[i + 1..i + 6]);
                boards.push(builder.build().unwrap());
                i += 5;
            }
            i += 1;
        }
        boards
    }

    fn new(rows: [[usize; 5]; 5]) -> Board {
        Board {
            rows,
            selected: [[false; 5]; 5],
        }
    }

    fn select(&mut self, number: usize) -> bool {
        for (i, row) in self.rows.iter().enumerate() {
            if row.contains(&number) {
                let index = row.iter().enumerate().find(|(_, x)| x == &&number).map(|(i, _)| i).unwrap();
                self.selected[i][index] = true;
                if self.check_winning_condition(i, index) {
                    return true;
                }
            }
        }
        false
    }

    fn check_winning_condition(&self, row: usize, column: usize) -> bool {
        let mut won_in_row = true;
        for element in self.selected[row] {
            if element.not() {
                won_in_row = false;
                break;
            }
        }
        let mut won_in_column = true;
        for row in self.selected {
            if row[column].not() {
                won_in_column = false;
                break;
            }
        }
        won_in_row || won_in_column
    }

    fn calculate_score(&self, last_pick: usize) -> usize {
        let mut sum_unselected = 0;
        for (number_row, selected_row) in self.rows.iter().zip(self.selected) {
            for (element, selected) in number_row.iter().zip(selected_row) {
                if selected.not() {
                    sum_unselected += element;
                }
            }
        }
        return sum_unselected * last_pick;
    }
}

struct BoardBuilder {
    rows: Vec<[usize; 5]>,
}

impl BoardBuilder {
    fn new() -> BoardBuilder {
        BoardBuilder { rows: vec![] }
    }

    fn add_rows(&mut self, rows: &[String]) {
        for row in rows {
            self.add_row_as_str(row)
        }
    }

    fn add_row_as_str(&mut self, row: &String) {
        let split: Vec<&str> = row.split_whitespace().collect();
        self.add_row(&split)
    }
    fn add_row(&mut self, row: &Vec<&str>) {
        let numbers: Vec<usize> = row.iter().map(|x| helper::parse_to_usize(x)).collect();
        self.rows.push(convert_to_array5(numbers));
    }

    fn build(self) -> Option<Board> {
        if self.rows.len().eq(&5) {
            Some(Board::new(convert_to_array5(self.rows)))
        } else {
            None
        }
    }
}

impl BoardBuilder {}


fn convert_to_array5<T>(v: Vec<T>) -> [T; 5] {
    v.try_into()
        .unwrap_or_else(|v: Vec<T>| panic!("Expected a Vec of length {} but it was {}", 5, v.len()))
}
