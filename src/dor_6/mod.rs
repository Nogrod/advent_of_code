use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        simulate_population(80)

    }

    fn part_2(&self) -> String {
        simulate_population(256)
    }
}

fn simulate_population(days_to_simulate: usize) -> String{
    let mut inputs = helper::get_lines_from_file(6);

    let raw_population: Vec<usize> = inputs.next().unwrap().split(",").
        map(|x| helper::parse_to_usize(&x)).collect();

    let mut population = [0; 9];
    for fish in raw_population {
        population[fish] += 1;
    }
    for _ in 0..days_to_simulate {
        simulate_day(&mut population);
    }
    let population_size: usize = population.iter().sum();
    format!("{:?}", population_size)
}

fn simulate_day(population: &mut [usize; 9]) {
    let num_new_fish = population[0];
    for i in 0..6 {
        population.swap(i, i+1);
    }
    population[6] += population[7];
    population[7] = population[8];
    population[8] = num_new_fish;
}