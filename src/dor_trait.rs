pub trait EntryPoint {
    fn part_1(&self) -> String;
    fn part_2(&self) -> String;
}