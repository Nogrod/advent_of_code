use std::fs::File;
use std::io::{BufReader, Lines};
use std::iter::Map;
use crate::dor_trait::EntryPoint;
use crate::helper;


mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(3);

        let mut diagnostic_report = to_32_vector(inputs);
        let num_elements = diagnostic_report.len() as u32;

        let mut gama_bits = diagnostic_report.pop().unwrap();
        for element in &diagnostic_report {
            gama_bits = gama_bits.into_iter().zip(element).map(|(x, y)| x + y).collect();
        }

        // Calculate each bit of the gamma_rate
        let mut result = vec![];
        for bit in gama_bits {
            if bit.gt(&(num_elements / 2)) {
                result.push(1);
            } else {
                result.push(0);
            }
        }

        let gamma_rate = bit_vec_to_u32(&result);

        let epsilon_rate = bit_vec_invert_to_u32(&result);

        let power_consumption = gamma_rate * epsilon_rate;

        format!("{}", power_consumption)
    }

    fn part_2(&self) -> String {
        let inputs = helper::get_lines_from_file(3);

        let diagnostic_report = to_32_vector(inputs);

        let oxygen_generator_rating = calculate_rating(diagnostic_report.clone(), 1);
        let co2_scrubber_rating = calculate_rating(diagnostic_report.clone(), 0);

        let life_support_rating = oxygen_generator_rating * co2_scrubber_rating;
        format!("{}", life_support_rating)
    }
}

fn bit_vec_to_u32(input: &Vec<u32>) -> u32 {
    let strings: Vec<String> = input.into_iter().map(|x| x.to_string()).collect();
    let single_string: String = strings.join("");
    u32::from_str_radix(&single_string, 2).unwrap()
}

fn bit_vec_invert_to_u32(input: &Vec<u32>) -> u32 {
    let strings: Vec<String> = input.into_iter().map(|x| invert(*x).to_string()).collect();
    let single_string: String = strings.join("");
    u32::from_str_radix(&single_string, 2).unwrap()
}

fn calculate_rating(mut diagnostic_report: Vec<Vec<u32>>, most_common_value: u32) -> u32 {
    for index in 0..diagnostic_report[0].len() {
        let ones = count_ones(&diagnostic_report, index);
        let bit_to_keep = if ones.ge(&(diagnostic_report.len() - ones))  {
            most_common_value
        } else {
            invert(most_common_value)
        };
        diagnostic_report = diagnostic_report.into_iter().filter(|x| x[index] == bit_to_keep).collect();

        if diagnostic_report.len().le(&1) {
            break
        }
    }
    bit_vec_to_u32(&diagnostic_report[0])
}

fn invert(number: u32) -> u32 {
    // Changes a "0" to a "1" and a "1" to a "0"
    number ^ 1
}


fn count_ones(elements: &Vec<Vec<u32>>, index: usize) -> usize {
    let mut number_of_ones = 0;
    for element in elements {
        number_of_ones += element[index]
    }
    return number_of_ones as usize;
}


fn to_32_vector(inputs: Map<Lines<BufReader<File>>, fn(std::io::Result<String>) -> String>) -> Vec<Vec<u32>> {
    let mut converted_inputs = vec![];
    for input in inputs {
        let digits: Vec<u32> = input.chars().into_iter().map(|x| x.to_digit(10).unwrap()).collect();
        converted_inputs.push(digits);
    }
    converted_inputs
}
