use std::collections::HashSet;
use itertools::Itertools;
use crate::dor_trait::EntryPoint;
use crate::helper;
use crate::helper::parse_to_usize;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let (mut points, folds) = get_points_and_folds();

        points = fold_paper(points, &folds[0].0, folds[0].1);

        format!("{}", points.len())
    }

    fn part_2(&self) -> String {
        let (mut points, folds) = get_points_and_folds();

        for fold in folds {
            points = fold_paper(points, &fold.0, fold.1);
        }

        format!("{}", get_paper(&points))
    }
}

fn get_paper(points: &HashSet<[usize; 2]>) -> String {
    let (mut max_x, mut max_y) = (0, 0);
    for point in points {
        if point[0].gt(&max_x) {
            max_x = point[0]
        }
        if point[1].gt(&max_y) {
            max_y = point[1]
        }
    }
    let mut paper = "\n".to_string();
    for y in 0..(max_y + 1) {
        let mut line = "".to_string();
        for x in 0..(max_x + 1) {
            if points.contains(&[x, y]) {
                line.push('#');
            } else {
                line.push('.');
            }
        }
        line.push('\n');
        paper.push_str(&line);
    }
    return paper;
}


fn fold_paper(points: HashSet<[usize; 2]>, axis: &str, fold_position: usize) -> HashSet<[usize; 2]> {
    let axis = match axis {
        "x" => 0,
        "y" => 1,
        &_ => { panic!("Invalid type of Line") }
    };

    let mut folded_points = HashSet::new();
    for point in points.into_iter() {
        if point[axis].gt(&fold_position) {
            let mut new_point = point.clone();
            new_point[axis] -= 2 * (new_point[axis] - fold_position);
            folded_points.insert(new_point);
        } else {
            folded_points.insert(point);
        }
    }
    folded_points
}

fn get_points_and_folds() -> (HashSet<[usize; 2]>, Vec<(String, usize)>) {
    let inputs = helper::get_lines_from_file(13);
    let mut points = HashSet::new();
    let mut folds = vec![];
    let mut in_points = true;
    for line in inputs {
        if line.is_empty() {
            in_points = false;
            continue;
        }

        if in_points {
            let point: (usize, usize) = line.split(',').map(|x| parse_to_usize(x)).next_tuple().unwrap();
            points.insert([point.0, point.1]);
        } else {
            let fold: (&str, &str) = line.split_whitespace().last().unwrap().split('=').next_tuple().unwrap();
            folds.push((fold.0.to_owned(), parse_to_usize(fold.1)))
        }
    }

    return (points, folds);
}