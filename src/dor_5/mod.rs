use std::collections::HashMap;
use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        count_dangerous_vents(false)
    }

    fn part_2(&self) -> String {
        count_dangerous_vents(true)
    }
}

fn count_dangerous_vents(consider_diagonal: bool) -> String {
    let inputs = helper::get_lines_from_file(5);
    let mut vent_map = HashMap::new();
    for raw_vent in inputs {
        for point in get_point_range(parse_vent(raw_vent), consider_diagonal) {
            let counter = vent_map.entry(point).or_insert(0);
            *counter += 1;
        }
    }
    let mut dangerous_vents = 0;
    for value in vent_map.values() {
        if value.ge(&2) {
            dangerous_vents += 1;
        }
    }

    format!("{}", dangerous_vents)
}


fn get_point_range(points: ((usize, usize), (usize, usize)), consider_diagonal: bool) -> Vec<(usize, usize)> {
    let x_range: Vec<usize> = if points.0.0.gt(&points.1.0) {
        (points.1.0..(points.0.0 + 1)).collect()
    } else {
        (points.0.0..(points.1.0 + 1)).rev().collect()
    };

    let y_range: Vec<usize> = if points.0.1.gt(&points.1.1) {
        (points.1.1..(points.0.1 + 1)).collect()
    } else {
        (points.0.1..(points.1.1 + 1)).rev().collect()
    };


    let point_range: Vec<(&usize, &usize)> = if x_range.len().eq(&1) {
        y_range.iter().map(|y| (&x_range[0], y)).collect()
    } else if y_range.len().eq(&1) {
        x_range.iter().map(|x| (x, &y_range[0])).collect()
    } else {
        if consider_diagonal {
            x_range.iter().zip(&y_range).collect()
        } else {
            vec![]
        }
    };

    let point_range: Vec<(usize, usize)> = point_range.iter().map(|(x, y)| (**x, **y)).collect();

    point_range
}

fn parse_vent(raw_input: String) -> ((usize, usize), (usize, usize)) {
    let mut parts: Vec<&str> = raw_input.split_whitespace().collect();
    let point_2 = parse_point(parts.remove(2));
    let point_1 = parse_point(parts.remove(0));
    return (point_2, point_1);
}

fn parse_point(raw_point: &str) -> (usize, usize) {
    let mut point: Vec<usize> = raw_point.split(",").map(|x| helper::parse_to_usize(x)).collect();
    return (point.remove(0), point.remove(0));
}