use std::collections::HashMap;
use itertools::Itertools;
use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        format!("{}", solution(10))
    }

    fn part_2(&self) -> String {
        format!("{}", solution(40))
    }
}

fn solution(num_iterations: usize) -> usize {
    let mut inputs = helper::get_lines_from_file(14);

    let polymer_template = inputs.next().unwrap();

    let mut polymer_template_map = HashMap::new();
    for element in polymer_template.chars().zip(polymer_template.chars().skip(1)) {
        add_new_pair(&mut polymer_template_map, element, &1)
    }

    inputs.next();

    let mut pair_insertion: HashMap<(char, char), char> = HashMap::new();
    for line in inputs {
        let pair: (&str, &str) = line.split(" -> ").next_tuple().unwrap();
        pair_insertion.insert(pair.0.chars().next_tuple().unwrap(), pair.1.chars().next().unwrap().to_owned());
    }


    for _ in 0..num_iterations {
        let mut new_entries = HashMap::new();
        for (pair, result) in pair_insertion.iter() {
            if pair_insertion.contains_key(&pair) {
                if let Some(num_pairs) = polymer_template_map.get(&pair) {
                    add_new_pair(&mut new_entries, (pair.0, *result), num_pairs);
                    add_new_pair(&mut new_entries, (*result, pair.1), num_pairs);
                }
            }
        }
        polymer_template_map = new_entries;
    }

    let mut count_elements = HashMap::new();
    for (pair, num) in polymer_template_map {
        let entry = count_elements.entry(pair.0).or_insert(0);
        *entry += num;
        let entry = count_elements.entry(pair.1).or_insert(0);
        *entry += num;
    }
    for entry in count_elements.values_mut() {
        if (*entry % 2).eq(&1) {
            *entry += 1;
        }
        *entry /= 2;
    }

    let max = count_elements.values().max().unwrap();
    let min = count_elements.values().min().unwrap();
    return  max - min
}


fn add_new_pair(polymer_template: &mut HashMap<(char, char), usize>, element: (char, char), num: &usize) {
    let entry = polymer_template.entry(element).or_insert(0);
    *entry += num;
}