use crate::dor_trait::EntryPoint;
use crate::helper;
use crate::helper::parse_to_i32;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let position = Position::new();

        drive(position)
    }

    fn part_2(&self) -> String {
        let position = AimPosition::new();

        drive(position)
    }
}

fn drive<I: Pos>(mut position: I) -> String{
    let inputs = helper::get_lines_from_file(2);
    for command in inputs {
        let command: String = command;
        let command: Vec<&str> = command.split_whitespace().collect();
        let order = command[0];
        let amount = parse_to_i32(command[1]);

        position.execute_order(order, amount);
    }
    return format!("{}", position.get_position())
}

struct Position {
    horizontal_position: i32,
    depth: i32,
}

impl Position {
    fn new() -> Position {
        Position {
            horizontal_position: 0,
            depth: 0,
        }
    }
}

impl Pos for Position {
    fn up(&mut self, amount: i32) {
        self.depth -= amount;
    }
    fn down(&mut self, amount: i32) {
        self.depth += amount;
    }
    fn forward(&mut self, amount: i32) {
        self.horizontal_position += amount;
    }
    fn get_position(&self) -> i32 {
        self.depth * self.horizontal_position
    }
}


struct AimPosition {
    horizontal_position: i32,
    depth: i32,
    aim: i32,
}

impl AimPosition {
    fn new() -> AimPosition {
        AimPosition {
            horizontal_position: 0,
            depth: 0,
            aim: 0,
        }
    }
}

impl Pos for AimPosition {
    fn up(&mut self, amount: i32) {
        self.aim -= amount;
    }
    fn down(&mut self, amount: i32) {
        self.aim += amount;
    }
    fn forward(&mut self, amount: i32) {
        self.horizontal_position += amount;
        self.depth += self.aim * amount
    }
    fn get_position(&self) -> i32 {
        self.depth * self.horizontal_position
    }
}

pub trait Pos {
    fn up(&mut self, amount: i32);
    fn down(&mut self, amount: i32);
    fn forward(&mut self, amount: i32);
    fn get_position(&self) -> i32;

    fn execute_order(&mut self, order: &str, amount: i32) {
        match order {
            "up" => { self.up(amount) }
            "down" => { self.down(amount) }
            "forward" => { self.forward(amount) }
            &_ => {}
        }
    }

}
