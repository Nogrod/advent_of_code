use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use std::iter::Map;
use std::str::FromStr;
use itertools::Itertools;


pub fn get_lines_from_file(day: i8) -> Map<Lines<BufReader<File>>, fn(std::io::Result<String>) -> String> {
    let path = format!("src/dor_{}/puzzle_input", day);
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);

    reader.
        lines().
        map(|x| x.unwrap())
}

pub fn parse_to_i32(string: &str) -> i32 {
    return i32::from_str(&string).unwrap();
}

pub fn parse_to_usize(string: &str) -> usize {
    return usize::from_str(&string).unwrap();
}

pub fn sort_string(input: &str) -> String {
    input.chars().sorted().collect::<String>()
}


pub fn to_u32_list(input: &str) -> Vec<u32> {
    let a = input.chars().map(|x| x.to_digit(10).unwrap()).collect();
    a
}
