use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor{
    fn part_1(&self) -> String {
        let measurements = helper::get_lines_from_file(1);

        let measurements = measurements.
            map(|x| helper::parse_to_i32(&x));

        let result = count_increasing_entries(measurements);
        format!("{}", result)
    }

    fn part_2(&self) -> String {
        let mut measurements = helper::get_lines_from_file(1);
        let mut three_measurement_sliding_window = vec![];

        let mut previous_measurements = [
            helper::parse_to_i32(&measurements.next().unwrap()),
            helper::parse_to_i32(&measurements.next().unwrap()),
        ];

        for line in measurements {
            let measurement = helper::parse_to_i32(&line);

            three_measurement_sliding_window.push(measurement + previous_measurements[0] + previous_measurements[1]);

            previous_measurements[1] = previous_measurements[0];
            previous_measurements[0] = helper::parse_to_i32(&line);
        }
        let result = count_increasing_entries(three_measurement_sliding_window.into_iter());
        format!("{}", result)
    }
}

fn count_increasing_entries<I: Iterator<Item=i32>>(mut elements: I) -> i32 {
    let mut counter = 0;

    let mut previous_element = elements.next().unwrap();

    for element in elements {
        if element.gt(&previous_element) {
            counter += 1;
        }

        previous_element = element;
    }
    counter
}
