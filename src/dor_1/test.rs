#[cfg(test)]
mod tests {
    use crate::dor_1;
    use crate::dor_trait::EntryPoint;

    #[test]
    fn test_solution_1() {
        let problem = dor_1::Dor {};
        assert_eq!(&problem.part_1(), "1462")
    }

    #[test]
    fn test_solution_2() {
        let problem = dor_1::Dor {};
        assert_eq!(&problem.part_2(), "1497")
    }
}
