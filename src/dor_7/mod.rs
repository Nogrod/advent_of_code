use std::collections::HashMap;
use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let mut inputs = helper::get_lines_from_file(7);
        let mut crap_population: Vec<usize> = inputs.next().unwrap().split(",").
            map(|x| helper::parse_to_usize(&x)).collect();
        crap_population.sort();

        let mut total_fuel_cost: usize = crap_population.iter().sum();
        let max_full_cost = *crap_population.iter().max().unwrap();

        let mut population = HashMap::new();
        for crap in &crap_population {
            let entry = population.entry(*crap).or_insert(0);
            *entry += 1;
        }
        let mut num_per_fuel_left = 0;
        let mut num_per_fuel_right = crap_population.len();
        let mut min_full_cost = total_fuel_cost;
        for full_cost in 0..(max_full_cost + 1) {
            let num_in_population = *population.entry(full_cost).or_insert(0);
            num_per_fuel_left = num_per_fuel_left + num_in_population;
            num_per_fuel_right = num_per_fuel_right - num_in_population;
            total_fuel_cost = total_fuel_cost + num_per_fuel_left - num_per_fuel_right;

            if total_fuel_cost.lt(&min_full_cost) {
                min_full_cost = total_fuel_cost;
            }
        }
        
        format!("{}", min_full_cost)
    }

    fn part_2(&self) -> String {
        let mut inputs = helper::get_lines_from_file(7);
        let mut crap_population: Vec<usize> = inputs.next().unwrap().split(",").
            map(|x| helper::parse_to_usize(&x)).collect();
        crap_population.sort();

        let max_full_cost = *crap_population.iter().max().unwrap();

        let mut min_full_cost = usize::MAX;

        for position in 0..(max_full_cost + 1) {
            let mut total_fuel_cost = 0;
            for crap in &crap_population {
                let distance = (*crap as i64 - position as i64).abs() as usize;
                total_fuel_cost += (distance.pow(2) + distance) / 2;
            }

            if total_fuel_cost.lt(&min_full_cost) {
                min_full_cost = total_fuel_cost;
            }
        }

        format!("{}", min_full_cost)
    }
}


    /*for position in 0..(max_full_cost + 1) {
        let b: usize = crap_population.iter().map(|x| ((*x as i64) - (position as i64)).abs() as usize).sum();
        let a = num_craps * position.pow(2) + sum_pos_squared + b - (2 * position + sum_pos);
        total_fuel_cost = (a as f64) / 2.;
        println!("{}", total_fuel_cost);

        if total_fuel_cost.lt(&min_full_cost) {
            min_full_cost = total_fuel_cost;
        }
    }
    println!();*/
