use std::collections::{HashMap, HashSet};
use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(12);

        let graph = build_graph(inputs.collect());
        let mut path = Path::new();
        let num_unique_paths = path.start(&graph, false);

        format!("{}", num_unique_paths)
    }

    fn part_2(&self) -> String {
        let inputs = helper::get_lines_from_file(12);

        let mut path = Path::new();
        let graph = build_graph(inputs.collect());
        let num_unique_paths = path.start(&graph, true);

        format!("{}", num_unique_paths)
    }
}

fn build_graph(inputs: Vec<String>) -> HashMap<String, Vec<String>> {
    let mut graph = HashMap::new();
    for line in inputs {
        let edge: Vec<&str> = line.split('-').collect();
        let entry = graph.entry(edge[0].to_string()).or_insert(vec![]);
        (*entry).push(edge[1].to_string());
        let entry = graph.entry(edge[1].to_string()).or_insert(vec![]);
        (*entry).push(edge[0].to_string());
    }
    graph
}

struct Path {
    visits_smale_caves: HashSet<String>,
}


impl Path {
    fn new() -> Path {
        Path {
            visits_smale_caves: HashSet::new(),
        }
    }

    fn from(visits: &HashSet<String>) -> Path {
        let visits = visits.clone();
        Path {
            visits_smale_caves: visits,
        }
    }

    fn start(&mut self, graph: &HashMap<String, Vec<String>>, can_visit_one_small_cave_twice: bool) -> usize {
        let count = self.update(graph, "start", can_visit_one_small_cave_twice);
        return count;
    }

    fn update(&mut self, graph: &HashMap<String, Vec<String>>, new_element: &str, mut can_visit_one_small_cave_twice: bool) -> usize {

        if  new_element.to_ascii_lowercase().eq(&new_element) {
            if self.visits_smale_caves.contains(new_element) {
                if !can_visit_one_small_cave_twice || new_element.eq("start") || new_element.eq("end") {
                    return 0;
                } else {
                    can_visit_one_small_cave_twice = false
                }
            } else {
                self.visits_smale_caves.insert(new_element.to_string());
            }
        }

        if new_element.eq("end") {
            return 1;
        }

        let mut count = 0;
        let neighbors = graph.get(new_element).unwrap();
        for next_element in &neighbors[1..] {
            let mut path = Path::from(&self.visits_smale_caves);
            count += path.update(graph, next_element, can_visit_one_small_cave_twice);
        }
        count += self.update(graph, &neighbors[0], can_visit_one_small_cave_twice);

        return count;
    }
}
