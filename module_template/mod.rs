use crate::dor_trait::EntryPoint;
use crate::helper;

mod test;

pub(crate) struct Dor {}


impl EntryPoint for Dor {
    fn part_1(&self) -> String {
        let inputs = helper::get_lines_from_file(0);

        format!("{}", "TODO")
    }

    fn part_2(&self) -> String {
        // let inputs = helper::get_lines_from_file(0);

        format!("{}", "TODO")
    }
}
