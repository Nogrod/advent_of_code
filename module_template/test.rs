#[cfg(test)]
mod tests {
    use crate::dor_X::Dor;
    use crate::dor_trait::EntryPoint;

    #[test]
    fn test_solution_1() {
        let problem = Dor {};
        assert_eq!(&problem.part_1(), "TODO INPUTS")
    }

    #[test]
    fn test_solution_2() {
        let problem = Dor {};
        assert_eq!(&problem.part_2(), "TODO INPUTS")
    }
}