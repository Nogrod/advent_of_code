#!/usr/bin/env shad

DAY=$(date +%e)
DAY_BEFORE=$(date --date="yesterday" +%e)

cp -r module_template src/dor_"${DAY}"

# Set the day for reading the correct file
sed -i "11s/.*/        let inputs = helper::get_lines_from_file(${DAY});/" src/dor_"${DAY}"/mod.rs
sed -i "17s/.*/        \/\/ inputs = helper::get_lines_from_file(${DAY});/" src/dor_"${DAY}"/mod.rs

# Set the import for the test
sed -i "3s/.*/    use crate::dor_${DAY}::Dor;/" src/dor_"${DAY}"/test.rs

# Include new module in main.rs
sed -i "s/^mod dor_${DAY_BEFORE};/mod dor_${DAY_BEFORE};\nmod dor_${DAY};/" src/main.rs
sed -i "s/^        Box::new(dor_${DAY_BEFORE}::Dor {}),/        Box::new(dor_${DAY_BEFORE}::Dor {}),\n        Box::new(dor_${DAY}::Dor {}),/" src/main.rs

git add src/dor_"$DAY"
