# Advent of Code

These are proposed solutions for the [Advent of Code 2021](https://adventofcode.com/2021) written in [Rust](https://www.rust-lang.org/).

For every solution there is a corosponding test.
